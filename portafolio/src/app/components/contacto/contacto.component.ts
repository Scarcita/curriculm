import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  forma! : FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
    this.forma = this.fb.group({
      nombre: ['',[ Validators.required, Validators.minLength(4) ]],
      apellido: ['', [Validators.required, Validators.maxLength(4)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      celular: ['', [Validators.required, Validators.pattern("0-9")]],
      mensaje: ['', [Validators.required]]
      
    });
   }

  ngOnInit(): void {

  }
   
  crearFormulario(): void {
  }

  guardar(): void{
    console.log(this.forma.value);
  }

  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }
}
